package relay42.test.time;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.Instant;


@RestController 

public class MainController {

  @RequestMapping("/")
  public String home(){
    return "Welcome to Spring-boot home page";
  }

  @RequestMapping("/time")
  public String time(){
    Instant now = Instant.now();
    String nowString = now.toString();
    return nowString;
  }

}