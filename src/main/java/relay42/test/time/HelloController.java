package relay42.test.time;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.Instant;


@RestController

public class HelloController {

  @Value("${server.port}") private String sport;
  @Value("${azname}") private String azname;
  
  @RequestMapping("/hello")
  public String serverInfo(){

    Instant now = Instant.now();
    String nowString = now.toString();

    return "hello " + sport + " " + azname + " " + nowString;
  }

}